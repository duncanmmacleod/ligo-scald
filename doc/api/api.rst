.. _api:

ligo-scald API
##############

.. toctree::
    :maxdepth: 2

    aggregator
    deploy
    io/io
    mock
    report
    serve
    utils
