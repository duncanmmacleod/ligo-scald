============
Installation
============

.. contents::
   :local:


conda installation
~~~~~~~~~~~~~~~~~~

You need a working conda installation. Get the correct miniconda for
your system from `here <https://conda.io/miniconda.html>`__.

To install with conda, run:

.. code:: bash

    conda install -c conda-forge ligo-scald

From source
~~~~~~~~~~~

If you want to use the most recent additions to ligo-scald or prefer not using
pip/conda to manage your python packages, you should install ligo-scald from source.

Prerequisites
^^^^^^^^^^^^^

+---------------------+-----------------+
| **Name**            | **Min Version** |
+---------------------+-----------------+
| ``bottle``          | 0.12            |
+---------------------+-----------------+
| ``h5py``            | 2.3             |
+---------------------+-----------------+
| ``ligo-common``     | 1.0             |
+---------------------+-----------------+
| ``numpy``           | 1.7             |
+---------------------+-----------------+
| ``python-dateutil`` | 1.0             |
+---------------------+-----------------+
| ``PyYAML``          | 3.10            |
+---------------------+-----------------+
| ``urllib3``         | 1.10            |
+---------------------+-----------------+

Install
^^^^^^^

.. code:: bash

    git clone https://git.ligo.org/gstlal-visualisation/ligo-scald.git
    cd ligo-scald
    python setup.py install --prefix=path/to/install/dir

Make sure to update your PATH and PYTHONPATH accordingly.
