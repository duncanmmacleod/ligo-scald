%define name              ligo-scald
%define version           0.7.2
%define unmangled_version 0.7.2
%define release           1

Summary:   SCalable Analytics for Ligo/virgo/kagra Data
Name:      %{name}
Version:   %{version}
Release:   %{release}%{?dist}
Source0:   http://software.ligo.org/lscsoft/source/%{name}-%{unmangled_version}.tar.gz
License:   GPLv2+
Group:     Development/Libraries
Prefix:    %{_prefix}
Vendor:    Patrick Godwin <patrick.godwin@ligo.org>
Url:       https://git.ligo.org/gstlal-visualisation/ligo-scald

BuildArch: noarch

BuildRequires: rpm-build
BuildRequires: epel-rpm-macros
BuildRequires: python-rpm-macros

# python2-ligo-scald
BuildRequires: python2-rpm-macros
BuildRequires: python2
BuildRequires: python2-setuptools

# python3-ligo-scald
BuildRequires: python3-rpm-macros
BuildRequires: python%{python3_pkgversion}
BuildRequires: python%{python3_pkgversion}-setuptools

%description
ligo-scald is a gravitational-wave monitoring and dynamic data visualization
tool.

# -- ligo-scald

Requires: python%{python3_pkgversion}-ligo-scald

%description
ligo-scald is a gravitational-wave monitoring and dynamic data visualization
tool.  This package provides the `scald` command-line interface.

# -- python2-ligo-scald

%package -n python2-%{name}
Summary:  %{summary}
Requires: python-dateutil
Requires: python-future
Requires: python-six
Requires: python-urllib3
Requires: python2-bottle
Requires: python2-h5py
Requires: python2-ligo-common
Requires: python2-numpy
Requires: python2-pyyaml

%{?python_provide:%python_provide python2-%{name}}

%description -n python2-%{name}
ligo-scald is a gravitational-wave monitoring and dynamic data visualization
tool.  This package provides the Python %{python2_version} library.

# -- python3-ligo-scald

%package -n python%{python3_pkgversion}-%{name}
Summary:  %{summary}
Requires: python%{python3_pkgversion}-bottle
Requires: python%{python3_pkgversion}-dateutil
Requires: python%{python3_pkgversion}-future
Requires: python%{python3_pkgversion}-h5py
Requires: python%{python3_pkgversion}-ligo-common
Requires: python%{python3_pkgversion}-numpy
Requires: python%{python3_pkgversion}-PyYAML
Requires: python%{python3_pkgversion}-six
Requires: python%{python3_pkgversion}-urllib3

%{?python_provide:%python_provide python%{python3_pkgversion}-%{name}}

%description -n python%{python3_pkgversion}-%{name}
ligo-scald is a gravitational-wave monitoring and dynamic data visualization
tool.  This package provides the Python %{python3_version} library.

# -- build steps

%prep
%setup -n %{name}-%{unmangled_version}

%build
%py2_build
%py3_build

%install
# install python2 first to have {prefix}/bin/ populated by python3
%py2_install
%py3_install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%license LICENSE
%{_bindir}/scald

%files -n python2-%{name}
%license LICENSE
%{python2_sitelib}/*

%files -n python%{python3_pkgversion}-%{name}
%license LICENSE
%{python3_sitelib}/*
